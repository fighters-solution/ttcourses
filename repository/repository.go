package repository

import (
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type CourseRepo struct {
	db         *mgo.Database
	collection string
}

func NewCourseRepo(cfg *config.Config) (*CourseRepo, error) {
	logrus.Infof("main: configuring NoSQL")
	sess, err := mgo.Dial(cfg.Database.URL)
	sess.SetMode(mgo.Monotonic, true)
	if err != nil {
		return nil, err
	}
	return &CourseRepo{
		db:         sess.DB(cfg.Database.Database),
		collection: cfg.Database.Collection,
	}, nil
}

func (rr *CourseRepo) GetCourseByName(r string) (model.Course, error) {
	ret := model.Course{}
	err := rr.db.C(rr.collection).Find(bson.M{"name": r}).One(&ret)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func (rr *CourseRepo) GetCourseByNameIgnoreCase(r string) ([]model.Course, error) {
	ret := make([]model.Course, 0)
	regex := bson.M{"$regex": bson.RegEx{Pattern: `^` + r + `.*`, Options: "i"}}
	err := rr.db.C(rr.collection).Find(bson.M{"name": regex}).All(&ret)

	return ret, err
}

func (rr *CourseRepo) CreateCourse(r *model.Course) (*model.Course, error) {
	c := rr.db.C(rr.collection)
	err := c.Insert(r)
	return r, err
}

func (rr *CourseRepo) FindAllCourses() ([]model.Course, error) {
	ret := make([]model.Course, 0)
	err := rr.db.C(rr.collection).Find(bson.M{}).All(&ret)
	return ret, err
}

func (rr *CourseRepo) Vote(p model.Course) (model.Course, error) {
	logrus.Info("Course Repo Vote")
	err := rr.db.C(rr.collection).Update(
		bson.M{"name": p.Name},
		bson.M{"$set": bson.M{"vote": p.Vote, "participants": p.Participants}},
	)
	logrus.Info("--- Error: ", err)
	if err != nil && strings.Contains(err.Error(), "not found") {
		return p, err
	}
	logrus.Info("--- MongoDB: Update successfully")
	return p, nil
}

func (rr *CourseRepo) FindHighestVote() (model.Course, error) {
	ret := model.Course{}
	err := rr.db.C(rr.collection).Find(bson.M{}).Sort("-vote").Limit(1).One(&ret)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func (rr *CourseRepo) DeleteCourseByName(r *model.Course) (*model.Course, error) {
	tmp, err := rr.GetCourseByName(r.Name)
	if err != nil {
		return nil, err
	}
	return &tmp, rr.db.C(rr.collection).Remove(bson.M{"name": r.Name})
}
