package controller

import (
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/jsonp"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/service"
	"golang.org/x/net/context"
)

var serv *service.CourseService

func Serve(cfg *config.Config) {
	r := chi.NewRouter()
	configureRouter(cfg, r)

	addr := cfg.Server.GetFullAddr()
	srv := &http.Server{
		Addr:         addr,
		Handler:      cors.Default().Handler(r),
		ReadTimeout:  cfg.Server.RTimeout,
		WriteTimeout: cfg.Server.WTimeout,
	}

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT, os.Interrupt, os.Kill)

	go func() {
		logrus.Info("---Main: Serving at http://%s", addr)
		logrus.Fatal(srv.ListenAndServe())
	}()

	serv = service.NewCourseService(cfg)

	// Graceful shutdown
	<-stop
	logrus.Info("---Main: Shutting down the server...")
	ctx, _ := context.WithTimeout(context.Background(), 3*time.Second)
	srv.Shutdown(ctx)
	logrus.Info("---Main:Goodbye!")
}

func configureRouter(cfg *config.Config, r *chi.Mux) {
	r.Use(middleware.DefaultLogger,
		middleware.DefaultCompress,
		middleware.Recoverer,
		middleware.Logger,
		jsonp.Handler)

	// Routing
	r.Route("/courses", func(r chi.Router) {
		r.Get("/", serv.FindAllCourses)
		r.Get("/{name}", serv.FindCoursesByName)
		r.Get("/alias/{alias}", serv.FindCourseByNameIgnoreCase)
		r.Get("/highest-vote", serv.FindHighestVote)
		r.Post("/", serv.CreateCourse)
		r.Post("/vote", serv.VoteCourse)
		r.Delete("/delete", serv.DeleteCourseByName)
	})
}
