package model

type (
	Course struct {
		Name         string        `bson:"name,omitempty" json:"name"`
		Description  string        `json:"description"`
		Author       string        `json:"author"`
		Image        string        `json:"image"`
		Vote         int           `json:"vote"`
		Participants []Participant `json:"participants"`
	}

	Participant struct {
		Id    string `json:"id"`
		Name  string `json:"name"`
		Email string `json:"email"`
	}
)
